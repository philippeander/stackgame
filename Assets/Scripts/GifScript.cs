﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GifScript : MonoBehaviour {
	[SerializeField] Sprite[] frames;
	[SerializeField] Image spriteTime;
	[SerializeField] float framesPorSegundo = 1.0f;
	float dTime;
	int frameNum;

	void Start(){
		spriteTime = GetComponent<Image> ();
		dTime = 0.0f;
		frameNum = 0;
	}

	void Update () {
		dTime += Time.deltaTime;
		if(framesPorSegundo < dTime){
			dTime = 0.0f;
			frameNum++;
			if (frameNum >= frames.Length)
				frameNum = 0;
			spriteTime.sprite = frames[frameNum];
		}
	}
}
