﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public SpawnerController spawner;
	public Text txtPoints;
	public GameObject[] feed;

	public GameObject TxtPlay;
	public GameObject TxtGameOver;

	public int LimitCount;
	public int numByRound;

    private static GameController gameController;

    int BlocosNum = 6;
    int roundNum = 1;
	int pontos;
	bool pause;

	public int Pontos{get{return pontos;} set{pontos = value;}}
	// Use this for initialization
	void Start () {
		pause = true;
		TxtPlay.SetActive (true);
		TxtGameOver.SetActive (false);
		pontos = 0;
        IniciarJogo();
		foreach(GameObject GO in feed){
			GO.SetActive (false);
		}
	}

    void IniciarJogo() {
        LimitCount = 0;
        BlocosNum = 6;
        roundNum = 1;
        spawner.StartMove(roundNum, BlocosNum);
    }

	// Update is called once per frame
	void Update () {

		if ( TxtPlay.activeSelf == true && Input.GetButtonDown ("Jump")) {
			pause = false;
			TxtPlay.SetActive(false);
		}
		else if (TxtPlay.activeSelf == false && Input.GetButtonDown("Jump"))
        {
            spawner.StopMove();
			numByRound = BlocosNum;
        }

		if(Input.GetKeyUp(KeyCode.Escape)){
			pause = !pause;
		}

		if(numByRound > 0 && LimitCount == numByRound){
			StartCoroutine (FeedTime(false));

		}

		if (pontos < 10) {
			txtPoints.text = "0" + pontos.ToString ();
		} else {
			txtPoints.text = pontos.ToString();
		}

		Pause ();
	
	}

    public static GameController Instance() {
        if(!gameController)
            gameController = FindObjectOfType(typeof(GameController)) as GameController;
        return gameController;
    }

    public void ToNextRound() {
		if(pontos > 3){
			GameObject.FindGameObjectWithTag("Base").transform.Translate(Vector3.down, Space.World);
		}
        
        roundNum++;
		BlocosNum = Random.Range (1, 6);
        spawner.StartMove(roundNum, BlocosNum ); 

    }

	public void VerifyError(){
        LimitCount++;


    }

	public void FeedTimeMet(){
		StartCoroutine (FeedTime(true));
	}

	IEnumerator FeedTime(bool state){
		if (!state) {
			feed [0].SetActive (true);
			TxtGameOver.SetActive (true);
			yield return new WaitForSeconds (4.0f);
			feed [0].SetActive (false);
			SceneManager.LoadScene (0);
		} else if(state){
			feed [1].SetActive (true);
			yield return new WaitForSeconds (2.0f);
			feed [1].SetActive (false);
			LimitCount = 0;
			numByRound = 0;
			ToNextRound ();
		}
	}

	void Pause(){
		if (pause) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}
}
