﻿using UnityEngine;
using System.Collections;

public class SpawnerController : MonoBehaviour {

    public int xLimit = 5;
    private int direction = -1;

    public float speedRate = 0.65f;
    public GameObject cubePrefab;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Move()
    {
        if (Mathf.Abs(this.transform.position.x) >= xLimit)
        {
            direction *= -1;
        }
        this.transform.Translate(Vector3.left * direction);

    }

    public void StartMove(int round, int numberOfCubes)
    {
        if (IsInvoking("Move"))
        {
            CancelInvoke("Move");
        }

        this.transform.position = new Vector3(xLimit - 1, 9, 0);

        for (int i = 0;  i < numberOfCubes; i++)
        {
            GameObject cubeClone = Instantiate(cubePrefab,
                new Vector3(this.transform.position.x + i,
                            this.transform.position.y,
                            this.transform.position.z), Quaternion.identity) as GameObject;
            cubeClone.name = "" + round + "-" + i;
            cubeClone.transform.SetParent(this.transform);
        } 
        float time = 1 * (Mathf.Pow(speedRate, round));
        if (Mathf.Abs(this.transform.position.x) >= xLimit)
        {
            direction *= -1;
        }
        InvokeRepeating("Move", time, time);
    }

    public void StopMove()
    {
        if (IsInvoking("Move"))
        {
            CancelInvoke("Move");
        }

        Rigidbody[] bodys = this.transform.GetComponentsInChildren<Rigidbody>();

        foreach (Rigidbody body in bodys)
        {
            body.useGravity = true;
        }

        this.transform.DetachChildren();

    }



}
