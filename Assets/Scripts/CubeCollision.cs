﻿using UnityEngine;
using System.Collections;

public class CubeCollision : MonoBehaviour
{
    public bool stacked = false;

    void Start()
    {
        stacked = false;
    }

    void OnCollisionEnter(Collision other)
    {
        if (!stacked)
        {
            RaycastHit MyRayHit;
            Ray MyRay = new Ray(this.transform.position, this.transform.up);
            Debug.DrawRay(this.transform.position, this.transform.up, Color.green,2);

            if (Physics.Raycast(MyRay, out MyRayHit, 1))
            {
                if (MyRayHit.collider != null)
				{
					stacked = true;
					GameController.Instance().Pontos += 1;
					GameController.Instance ().FeedTimeMet ();
					//GameController.Instance().ToNextRound();
                }
            }
        }


		if(other.gameObject.tag == "GameController"){
			GameController.Instance ().VerifyError ();
			Destroy (gameObject);
		}

    }
}
